package studio.epic.FaceTweetPlus;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class HelpFragment extends Fragment implements RatingBar.OnRatingBarChangeListener {


    public HelpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_help, null);

        RatingBar rb = (RatingBar) view.findViewById(R.id.ratingBar);
        rb.setNumStars(5);
        rb.setStepSize(1.0f);
        rb.setOnRatingBarChangeListener(this);

        return view;
    }



    @Override
    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
        Toast.makeText(getActivity(), "Thanks for Rating "+ Math.round(rating) +" stars", Toast.LENGTH_SHORT).show();
    }
}
