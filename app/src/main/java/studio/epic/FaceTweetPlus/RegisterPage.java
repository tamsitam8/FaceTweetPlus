package studio.epic.FaceTweetPlus;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class RegisterPage extends AppCompatActivity implements View.OnClickListener {

    public static String name, pass, email, gender, phone, hasDP = "true";
    private EditText userTxt, emailTxt, passTxt, phoneTxt;
    private RadioGroup radioGender;
    private Button signUpButton;
    private TextView LoginLink;
    public static ImageView uploadImage = null;
    private SQLiteHelper mSqlLiteHelper;
   // public static byte[] bytes;
    public static LoginPage mLoginPage;
    public static HomeActivity mHomeActivity;

    //For Profile Picture
    private static final int REQ_CAMERA = 5;
    private static final int REQ_GALLERY = 10;
    private int DPSetFrom = 0;
    private String picturePath = null;
    protected static String profileDir = "profile_pics";
    private static final String PREF_PROF_IMG_SIZE = "ProfileImgSizePref";
    private static final int DEF_IMG_SIZE = 70;

    public static final String PREFS_NAME = "myPref";
    public static final String PROFILE_PATH = "Profile_Path";
    public Bitmap bitmap = null;

    private AlertDialog.Builder builder;
    private AlertDialog alertDialog;
    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };


    //Profile picture Variables ends here

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_page);


        //Finding UI elements
        userTxt = (EditText) findViewById(R.id.userName);
        phoneTxt = (EditText) findViewById(R.id.userPhone);
        emailTxt = (EditText) findViewById(R.id.userEmail);
        passTxt = (EditText) findViewById(R.id.userPass);
        radioGender = (RadioGroup) findViewById(R.id.radioGender);
        signUpButton = (Button) findViewById(R.id.btn_signup);
        LoginLink = (TextView) findViewById(R.id.login_link);
        uploadImage = (ImageView) findViewById(R.id.profileUpload);
        //UI elements ends here

        mSqlLiteHelper = new SQLiteHelper(this);//Creates the Table

        mHomeActivity = new HomeActivity();
        mLoginPage = new LoginPage();


        //making UI elements clickable...
        LoginLink.setOnClickListener(this);
        uploadImage.setOnClickListener(this);
        signUpButton.setOnClickListener(this);
        //it ends here

        //Bounce Animation to logo
        findViewById(R.id.profileUpload).clearAnimation();
        TranslateAnimation translation;
        translation = new TranslateAnimation(0f, 0f, getDisplayHeight(), 0f);
        translation.setStartOffset(500);
        translation.setDuration(2000);
        translation.setFillAfter(true);
        translation.setInterpolator(new BounceInterpolator());
        findViewById(R.id.profileUpload).startAnimation(translation);
        //bounce animation ends here

    }

    //Make Register Button, Image Upload and LoginLink Clickable....
    @Override
    public void onClick(View view) {

        int ID = view.getId();

        //if user is already registered.
        if (ID == R.id.login_link) {
            alreadyRegistered();
            return;
        }

        //Getting the Profile Picture..
        if (ID == R.id.profileUpload) {
              //check for Null User.. otherwise it will create null_profile.jpg file :(
            if (userTxt.getText().toString().equals("")) {
                doAlert("Form Alert !", "Please Fill the Form first then Click the Upload Button");
                return;
            } else {
                showChooser();
                return;
            }
        }

        //User Clicked Register Button....
        if(ID == R.id.btn_signup) {
            name = userTxt.getText().toString();
            email = emailTxt.getText().toString();
            pass = passTxt.getText().toString();
            phone = phoneTxt.getText().toString();

            if (!validate()) {
                onSignupFailed();
                return;
            }

            final ProgressDialog progressDialog = new ProgressDialog(RegisterPage.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Creating Account...");
            progressDialog.show();


            //Create handler for ProgressDialog....extra Step done for efficency. dont burden the Main UI Thread :)
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            onSignupSuccess();
                            progressDialog.dismiss();
                        }
                    }, 3000);
        }
    }

    private void onSignupSuccess() {

        // Getting Gender from RadioGroup
        if (radioGender.getCheckedRadioButtonId() == R.id.male)
            gender = "Male";

        else if (radioGender.getCheckedRadioButtonId() == R.id.female)
            gender = "Female";


        //User Registered now store the DP in storage.
        if(DPSetFrom == REQ_CAMERA){
            try {
                saveToInternalStorage(bitmap);
                Toast.makeText(RegisterPage.this, "Profile picture saved Successfully from Camera", Toast.LENGTH_SHORT).show();

                hasDP = "true";

            } catch (IOException e) {
                Toast.makeText(RegisterPage.this, "Error in saving the picture", Toast.LENGTH_SHORT).show();
            }
        }

        if(DPSetFrom == REQ_GALLERY){
            try {
                saveToInternalStorage(BitmapFactory.decodeFile(picturePath));
                bitmap = BitmapFactory.decodeFile(picturePath);
                Toast.makeText(RegisterPage.this, "Profile picture saved Successfully from Gallery", Toast.LENGTH_SHORT).show();

                hasDP = "true";

            } catch (IOException e) {
                Toast.makeText(RegisterPage.this, "Error in saving the picture", Toast.LENGTH_SHORT).show();
            }
        }


        //Set hasDP to false if bitmap is null.
        if (bitmap == null) {
            Toast.makeText(RegisterPage.this, "You havent uploaded any profile picture!", Toast.LENGTH_SHORT).show();
            hasDP = "false";
        }

        Log.d("aaa", "if in SignUpSuccess bitmap = " + bitmap);
        Log.d("aaa", "if in SignUpSuccess hasDP = " + hasDP);

        //check if user was registered in database or not.!
        if (mSqlLiteHelper.insetToDB(name, pass, email, gender, phone,hasDP)) {
            Toast.makeText(RegisterPage.this, "Successfully registered user!", Toast.LENGTH_SHORT).show();

            startActivity(new Intent(this,LoginPage.class));
            finish();
        } else {
            Toast.makeText(RegisterPage.this, "Error in registering user!", Toast.LENGTH_SHORT).show();
            Log.d("aaa", "else in SignUpSuccess bitmap = " + bitmap);
            Log.d("aaa", "else in SignUpSuccess hasDP = " + hasDP);
        }
    }

    private void onSignupFailed() {
        Toast.makeText(getBaseContext(), "SignUp Error , code : (0xFFc3)", Toast.LENGTH_LONG).show();
        signUpButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        if (name.isEmpty() || name.length() < 4) {
            userTxt.setError("at least 4 characters");
            valid = false;
        } else {
            userTxt.setError(null);
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailTxt.setError("enter a valid email address");
            valid = false;
        } else {
            emailTxt.setError(null);
        }

        if (pass.isEmpty() || pass.length() < 4 || pass.length() > 10) {
            passTxt.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            passTxt.setError(null);
        }

        if (phone.length() < 10 || phone.length() > 10) {
            phoneTxt.setError("Phone No. cannot be less or greater than 10");
            valid = false;
        }

        if (radioGender.getCheckedRadioButtonId() == -1) {
            doAlert("Gender", "You haven't selected any Gender!" + "\n" + "Please Register Again!");
            valid = false;
        }

        return valid;
    }


    public void alreadyRegistered() {
        Log.d("aaa","in AlreadyRegistered home Called = "+mHomeActivity.homeIsCalled);
        if(mHomeActivity.homeIsCalled) {
            Log.d("aaa","in IF AlreadyRegistered home Called = "+mHomeActivity.homeIsCalled);
            if (mHomeActivity.userSignedOut) {
                Log.d("aaa","in IF IF AlreadyRegistered home Called = "+mHomeActivity.homeIsCalled);
                Log.d("aaa","in IF IF AlreadyRegistered userSigned out = "+mHomeActivity.userSignedOut);
                startActivity(new Intent(this, LoginPage.class));
                finish();
            } else {
                Toast.makeText(RegisterPage.this, "Please SignOut first !", Toast.LENGTH_LONG).show();
                finish();
            }
        }
        else {
            Log.d("aaa","in elsee AlreadyRegistered home Called = "+mHomeActivity.homeIsCalled);
            Log.d("aaa","in elsee AlreadyRegistered userSigned out = "+mHomeActivity.userSignedOut);
            startActivity(new Intent(this, LoginPage.class));
            finish();
        }
    }

    public void doAlert(String Title, String Message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(Title)
                .setMessage(Message)
                .setCancelable(false)
                .setNegativeButton("Okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    //For Profile Picture ........
    private String saveToInternalStorage(Bitmap bitmapImage) throws IOException {
        ContextWrapper cw = new ContextWrapper(getApplicationContext());

        // path to /data/data/yourapp/app_data/imageDir
        //File directory = cw.getDir("profileDir", Context.MODE_PRIVATE);

        //saving to /storage/0/android/profileDir/files/directory
        File directory = getExternalFilesDir(profileDir);

        // Create imageDir
        File mypath = new File(directory, mSqlLiteHelper.generateUID(userTxt.getText().toString()));

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.JPEG, getprofImageSize(), fos);

            //save the profile path to sharedPref
            if (directory != null) {
                getSharedPreferences(PREFS_NAME, MODE_PRIVATE)
                        .edit()
                        .putString(PROFILE_PATH, directory.getAbsolutePath())
                        .commit();
            }

            Toast.makeText(RegisterPage.this, "Image Saved in storage", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(RegisterPage.this, "" + e, Toast.LENGTH_LONG).show();
        } finally {
            if (fos != null) {
                fos.close();
            }
        }
            return directory.getAbsolutePath();
    }


    //verify Storage Permissions for API level 21 and above..
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }


    private int getprofImageSize() {
        //get value
        int imgSize = getSharedPreferences(PREFS_NAME, MODE_PRIVATE)
                .getInt(PREF_PROF_IMG_SIZE,DEF_IMG_SIZE);
        return imgSize;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ_CAMERA && resultCode == RESULT_OK) {
            openCamera(data);
            Toast.makeText(RegisterPage.this, "Picture Loaded from Camera!, DP not set yet! Please Register", Toast.LENGTH_SHORT).show();
            DPSetFrom = REQ_CAMERA;
        }

        if (requestCode == REQ_GALLERY && resultCode == RESULT_OK && data != null) {
            openGallery(data);
            Toast.makeText(RegisterPage.this, "Picture Loaded from Gallery! DP not set yet! Please Register", Toast.LENGTH_SHORT).show();
            DPSetFrom = REQ_GALLERY;
        }

    }


    private void openCamera(Intent data) {
        Bundle bundle = data.getExtras();
        bitmap = (Bitmap) bundle.get("data");

    }

    private void openGallery(Intent data) {
        Uri selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        Cursor cursor = getContentResolver().query(selectedImage,
                filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        picturePath = cursor.getString(columnIndex);
        cursor.close();

    }


    public void showChooser() {

        builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose Image from");

        LayoutInflater lf = getLayoutInflater();
        View chooserView = lf.inflate(R.layout.chooser_layout, null);
        builder.setView(chooserView);

        alertDialog = builder.create();
        alertDialog.show();

    }

    public void chooseFromCamera(View view) {
        Intent it = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(it, REQ_CAMERA);

        alertDialog.dismiss();
    }

    public void chooseFromGallery(View view) {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        verifyStoragePermissions(RegisterPage.this);
        startActivityForResult(i, REQ_GALLERY);

        alertDialog.dismiss();
    }
    //Profile Picture ends here....


    private int getDisplayHeight() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.widthPixels;
    }
}

