package studio.epic.FaceTweetPlus;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ShareCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.PlusShare;


public class GooglePlusFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks {

    private final String TAG = "gPlusLog";

    private static final int RESULT_OK = -1;
    private static final int RESULT_CANCELED = 0;
    private ProgressDialog mProgressDialog;
    private String status = null;

    //Signin Button
    private SignInButton gSignInButton;
    protected static String hStatus = null;

    //Signing options
    private GoogleSignInOptions gso;

    //Google api client
    protected static GoogleApiClient mGoogleApiClient;

    //Signin constant to check the activity result
    private int RC_SIGN_IN = 100;

    //Variable for TextView,Button and Network ImageView.
    private static TextView textViewName;
    private static TextView textViewEmail;
    private TextView gNameLabel,gEmailLabel;
    private EditText gStatusTxt;
    private static String instanceName = null, instanceEmail = null, gPlusImageURL = null;
    private Button gPostButton, gSignOut;
    private static NetworkImageView profilePhoto;

    //Image Loader
    private static ImageLoader imageLoader;
    private int GPOST_APP = 5;
    private final int GPOST_WEBVIEW = 10;
    protected static boolean gLoginResult = false;
    public static boolean isHLogOut = false;

    public GooglePlusFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("TAG", "in onCreate GPlusFrag isHLogOut = " + isHLogOut);

        //Initializing Google Signin option
        if (gso == null) {
            gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();

            if (isHLogOut == false) {
                try {
                    buildGoogleApiClient();
                } catch (IllegalStateException ex) {
                    Log.d(TAG, "buildGoogleApiClient :" + ex);
                }
            }
        }

    }

    @Override
    public void onStart() {
        super.onStart();


        Log.d("TAG", "in onStart GPlusFrag isHLogOut = " + isHLogOut);
        Log.d("TAG","in onStart GPlusFrag Hstatus = "+hStatus);

        if (hStatus != null && new HomeActivity().gPlusCheckBoxChecked) {
            gPlusPostStatus(hStatus);
            hStatus = null;
        }

        if (isHLogOut) {

            if (gLoginResult == true) {
                Log.d("TAG", "in onStart isHLogOut = " + isHLogOut);

                gAppSignOut();
                //disconnect GooglePlus services API also
                disconnectGoogleAPI();
            }
            return;
        }

        if (mGoogleApiClient != null && gLoginResult == false)
            mGoogleApiClient.connect();

        if (gLoginResult == true && new HomeActivity().onStartCalled == 0)
            gPlusSignIn();
        else {
            if (gLoginResult == true)
                try {
                    //setting name and email label invisible.
                    gNameLabel.setVisibility(View.VISIBLE);
                    gEmailLabel.setVisibility(View.VISIBLE);

                    textViewEmail.setText(instanceEmail);
                    textViewName.setText(instanceName);
                    profilePhoto.setImageUrl(gPlusImageURL, imageLoader);


                    gSignInButton.setEnabled(false);

                    gSignOut.setVisibility(View.VISIBLE);
                    gSignOut.setEnabled(true);

                    Log.d("TAG", "in onStart G+ instance imageLoader = " + imageLoader);
                    Log.d("TAG", "in onStart G+ instance gPlusImageURL = " + gPlusImageURL);
                } catch (NullPointerException ex) {
                    Log.d("TAG", "Exception inOnstart G+ frag instance " + ex);
                }

        }



        Log.d(TAG, "onStart called");
        Log.d(TAG, "mGoogleApiClient onStart = " + mGoogleApiClient);
        Log.d(TAG, "onStart mGoogleApiClient.isConnected() = " + mGoogleApiClient.isConnected());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_google_plus, null);

        //Initializing Views
        textViewName = (TextView) view.findViewById(R.id.textViewName);
        textViewEmail = (TextView) view.findViewById(R.id.textViewEmail);
        gNameLabel = (TextView) view.findViewById(R.id.gNameLbl);
        gEmailLabel = (TextView) view.findViewById(R.id.gEmailLbl);

        gStatusTxt = (EditText) view.findViewById(R.id.gplusShareTxt);
        gPostButton = (Button) view.findViewById(R.id.gPlusPost);
        gSignOut = (Button) view.findViewById(R.id.gSignOut);
        profilePhoto = (NetworkImageView) view.findViewById(R.id.profileImage);

        //setting name and email label invisible.
        gNameLabel.setVisibility(View.INVISIBLE);
        gEmailLabel.setVisibility(View.INVISIBLE);


        //setting progress dialog.
        mProgressDialog = new ProgressDialog(getActivity());

        //Initializing Signin Button
        gSignInButton = (SignInButton) view.findViewById(R.id.sign_in_button);
        gSignInButton.setSize(SignInButton.SIZE_WIDE);
        gSignInButton.setScopes(gso.getScopeArray());


        //Setting onclick listener to signIn button
        gSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gPlusSignIn();
                Log.d(TAG, "gLoginResult onClick = " + gLoginResult);
            }
        });

//        gSignOut.setVisibility(View.INVISIBLE);
//        gSignOut.setEnabled(false);

        //Setting onclick listener to SingOut Button button
        gSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mGoogleApiClient.isConnected())
                    gPlusSignOut();
            }
        });

        //Setting onclick listener to Post Button button
        gPostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                status = gStatusTxt.getText().toString();
                gPlusPostStatus(status);
            }
        });

        return view;
    }

    protected synchronized void buildGoogleApiClient() {
        //Initializing Google api client
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity().getApplicationContext())
                .enableAutoManage(getActivity(), new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult connectionResult) {
                        Log.d(TAG, "Connection failed " + connectionResult);
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addConnectionCallbacks(this)
                .build();

        Log.d(TAG, "mGoogleApiClient = " + mGoogleApiClient);
        Log.d(TAG, "onCreate mGoogleApiClient.isConnected() = " + mGoogleApiClient.isConnected());
    }

    //This function will option signing intent
    protected void gPlusSignIn() {

        //Creating an intent
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        //Starting intent for result
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }


    protected void gPlusSignOut() {

        Log.d("TAG", "gPlusSignOut caleed");
        if (gLoginResult == false) {
            Toast.makeText(thisActivity(), "Please Sign in first", Toast.LENGTH_SHORT).show();
            return;
        }

        // [START signOut]
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {

                        try {

                            if (status.isSuccess()) {
                                Toast.makeText(thisActivity(), "Successfully Signed out from Google Plus", Toast.LENGTH_SHORT).show();

                                //setting name and email label invisible.
                                gNameLabel.setVisibility(View.INVISIBLE);
                                gEmailLabel.setVisibility(View.INVISIBLE);

                                //Displaying name and email
                                textViewName.setText("");
                                textViewEmail.setText("");
                                profilePhoto.setImageResource(R.drawable.demo_user);

                                //setting SignIn button visible if logout was success.
                                gSignInButton.setVisibility(View.VISIBLE);
                                gSignInButton.setEnabled(true);

                                gSignOut.setVisibility(View.INVISIBLE);
                                gSignOut.setEnabled(false);

                                gLoginResult = false;

                            } else
                                Toast.makeText(thisActivity(), "Error in Signing out!", Toast.LENGTH_SHORT).show();
                        } catch (NullPointerException ex) {
                            Log.d(TAG, "Exception Gplus Logout :" + ex);
                        }

                    }
                });
        // [END signOut]

    }


    protected void gAppSignOut() {

        Log.d("TAG", "in G+ Frag gAppSignOut gLoginResult = " + gLoginResult);

        if (gLoginResult == false) {

            Log.d("TAG", "in G+ Frag gAppSignOut if called");

            Toast.makeText(thisActivity(), "Cannot Logout ,You are not signed with G+", Toast.LENGTH_SHORT).show();
            return;
        } else {
            Log.d("TAG", "in G+ Frag gAppSignOut else called");
            // [START signOut]
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            try {
                                Log.d("TAG", "in G+ Frag gAppSignOut if called onResult status = " + status);
                                if (status.isSuccess()) {
                                    Log.d("TAG", "in G+ Frag gAppSignOut if called onResult status.isSuccess = " + status);
                                    Toast.makeText(thisActivity(), "Successfully Signed out from Google Plus", Toast.LENGTH_SHORT).show();
                                    // gLoginResult = false;
                                } else
                                    Toast.makeText(thisActivity(), "Error in Signing out!", Toast.LENGTH_SHORT).show();
                            } catch (NullPointerException ex) {
                                Log.d(TAG, "Exception Gplus Logout :" + ex);
                            }

                        }
                    });
            // [END signOut]

        }
    }


    //    @Override
//    public void onPause() {
//        super.onPause();
//        mGoogleApiClient.stopAutoManage(getActivity());
//        mGoogleApiClient.disconnect();
//
//        Log.d(TAG, "onPause called");
//        Log.d(TAG,"mGoogleApiClient onPause = "+mGoogleApiClient);
//        Log.d(TAG,"onPause mGoogleApiClient.isConnected() = "+mGoogleApiClient.isConnected());
//    }
//
//
//    @Override
//    public void onStop() {
//        super.onStop();
//
//        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
//            mGoogleApiClient.disconnect();
//        }
//
//        Log.d(TAG, "onStop called");
//        Log.d(TAG,"mGoogleApiClient onStop = "+mGoogleApiClient);
//        Log.d(TAG,"onStop mGoogleApiClient.isConnected() = "+mGoogleApiClient.isConnected());
//    }

//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//
//        AppSignOut();
//        disconnectGoogleAPI();
//
//        Log.d(TAG, "onDestroy called");
//        Log.d(TAG, "mGoogleApiClient onDestroy = " + mGoogleApiClient);
//        Log.d(TAG, "onDestroy mGoogleApiClient.isConnected() = " + mGoogleApiClient.isConnected());
//    }

    protected void disconnectGoogleAPI() {

        try {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        } catch (NullPointerException ex) {
            Log.d(TAG, "Exception :" + ex);
        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //If signin
        if (requestCode == RC_SIGN_IN) {
            final GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

            new android.os.Handler().postDelayed(
                    new Runnable() {

                        //Background Thread
                        public void run() {

                            //your Background thread code here
                            //Calling a new function to handle signin
                            handleSignInResult(result);

                            //Foreground Thread
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //your Foreground UI thread code here
                                    //show the Login Progress.
                                    mProgressDialog.setTitle("GooglePlus Login!");
                                    mProgressDialog.setMessage("Authenticating Please wait...");
                                    mProgressDialog.show();
                                }
                            });
                        }

                    }, 1000);


        }

        if (requestCode == GPOST_APP && resultCode == RESULT_OK) {
            Toast.makeText(thisActivity(), "Status Posted Successfully from G+ App", Toast.LENGTH_SHORT).show();
            hStatus = null;
        }
        else if (requestCode == GPOST_WEBVIEW && resultCode == RESULT_OK) {
            Toast.makeText(thisActivity(), "Status Posted Successfully from G+ App", Toast.LENGTH_SHORT).show();
            hStatus = null;
        }
        else if (requestCode == GPOST_APP && resultCode == RESULT_CANCELED)
            Toast.makeText(thisActivity(), "You cancel posting status from G+ App", Toast.LENGTH_SHORT).show();

        else if (requestCode == GPOST_WEBVIEW && resultCode == RESULT_CANCELED)
            Toast.makeText(thisActivity(), "You cancel posting status from G+ App", Toast.LENGTH_SHORT).show();

        Log.d("TAG","in onActivityResult GPlusFrag Hstatus = "+hStatus);
    }

    //After the signing we are calling this function
    private void handleSignInResult(final GoogleSignInResult result) {


        //create seperate thread for Background and Forground processing.
        new android.os.Handler().postDelayed(
                new Runnable() {

                    //Background Thread
                    public void run() {

                        //If the login succeed
                        if (result.isSuccess()) {

                            gLoginResult = true;
                            Log.d("TAG", "in handleSignInResult  gLoginResult = " + gLoginResult);

                            //setting SignIn button invisible and signout button visible
                            //gSignInButton.setVisibility(View.INVISIBLE);
                            gSignInButton.setEnabled(false);

                            gSignOut.setVisibility(View.VISIBLE);
                            gSignOut.setEnabled(true);

                            //Getting google account
                            final GoogleSignInAccount acct = result.getSignInAccount();

                            //setting name and email label visible.
                            gNameLabel.setVisibility(View.VISIBLE);
                            gEmailLabel.setVisibility(View.VISIBLE);

                            //Displaying name and email
                            textViewName.setText(acct.getDisplayName());
                            textViewEmail.setText(acct.getEmail());


                            //save the instance of user email
                            instanceName = textViewName.getText().toString();
                            instanceEmail = textViewEmail.getText().toString();

                            try {
                                //Initializing image loader
                                imageLoader = CustomVolleyRequest.getInstance(thisActivity())
                                        .getImageLoader();

                                imageLoader.get(acct.getPhotoUrl().toString(),
                                        ImageLoader.getImageListener(profilePhoto,
                                                R.mipmap.app_launcher_icon,
                                                R.mipmap.app_launcher_icon));
                            }catch (NullPointerException ex){
                                Log.d("TAG","imageLoader = CustomVolleyRequest = "+ex);
                            }

                            //Update the UI with Image.
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //Loading image
                                    try {
                                        profilePhoto.setImageUrl(acct.getPhotoUrl().toString(), imageLoader);
                                        gPlusImageURL = acct.getPhotoUrl().toString();
                                        //profilePhoto.getImageURL().toString();

                                        Log.d("TAG", "setImageUrl G+ profilePhoto.getImageURL  = " + profilePhoto.getImageURL());
                                        Log.d("TAG", "setImageUrl G+ gPlusImageURL  = " + gPlusImageURL);
                                        Log.d("TAG", "setImageUrl G+ imageLoader  = " + imageLoader);

                                    } catch (NullPointerException ex) {
                                        Log.d("TAG", "Exception G+ frag profilePhoto.setImageUrl " + ex);
                                    }

                                    //show the success message.
                                    Toast.makeText(thisActivity(), "Login Success!", Toast.LENGTH_SHORT).show();
                                    mProgressDialog.dismiss();
                                }
                            });


                        } else {
                            //If login fails
                            Toast.makeText(thisActivity(), "Login Failed", Toast.LENGTH_LONG).show();
                            mProgressDialog.dismiss();
                            Log.d(TAG, "" + result);
                        }

                    }

                }, 3000);


        Log.d(TAG, "gLoginResult handle = " + gLoginResult);
        Log.d(TAG, "mGoogleApiClient handle = " + mGoogleApiClient);
        Log.d(TAG, "onCreate mGoogleApiClient.isConnected() = " + mGoogleApiClient.isConnected());
    }

    protected void gPlusPostStatus(String status) {

        Log.d(TAG, "gPlusPostStatus called ");

        Log.d(TAG, "gPlusPostStatus status = " + status);
        Log.d(TAG, "gPlusPostStatus gLoginResult = " + gLoginResult);

        if (gLoginResult == true) {

            PackageInfo gPlusAppInstalled = null;

            try {
                PackageManager pm = getActivity().getPackageManager();
                gPlusAppInstalled = pm.getPackageInfo("com.google.android.apps.plus", PackageManager.GET_META_DATA);
                Log.d(TAG, "gPlusAppInstalled = " + gPlusAppInstalled);

            } catch (PackageManager.NameNotFoundException e) {
                Toast.makeText(getActivity().getApplicationContext(), "Google+ App  not Installed", Toast.LENGTH_SHORT)
                        .show();
            } catch (NullPointerException ne) {
                Log.d(TAG, "NullPointer = " + ne);
            }

            Log.d(TAG, "gPlusAppInstalled = " + gPlusAppInstalled);

            if (gPlusAppInstalled != null) {
                Intent mIntent = ShareCompat.IntentBuilder.from(getActivity())
                        .setText(status).setType("text/plain")
                        .getIntent()
                        .setPackage("com.google.android.apps.plus");
                startActivityForResult(mIntent, GPOST_APP);
            } else {

                Intent shareIntent = new PlusShare.Builder(getActivity())
                        .setType("text/plain")
                        .setText(status)
                        .getIntent();

                if (isAdded()) {
                    Log.d(TAG, "isAdded " + getResources().getString(R.string.app_name));
                    startActivityForResult(shareIntent, GPOST_WEBVIEW);
                } else
                    Log.d(TAG, "Not Attacthed");
            }
        } else
            Toast.makeText(thisActivity(), "Please Sign In first to Google+!", Toast.LENGTH_SHORT).show();

    }


    protected void posthStatus() {
        gPlusPostStatus(hStatus);
    }


    private Context thisActivity() {
        return getActivity().getApplicationContext();
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "onConnected called");
        Log.d(TAG, "mGoogleApiClient onConnected = " + mGoogleApiClient);
        Log.d(TAG, "onConnected mGoogleApiClient.isConnected() = " + mGoogleApiClient.isConnected());
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended called");
        Log.d(TAG, "mGoogleApiClient onConnectionSuspended = " + mGoogleApiClient);
        Log.d(TAG, "onConnectionSuspended mGoogleApiClient.isConnected() = " + mGoogleApiClient.isConnected());
    }
}
