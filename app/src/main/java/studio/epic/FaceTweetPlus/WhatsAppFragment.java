package studio.epic.FaceTweetPlus;


import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class WhatsAppFragment extends Fragment {

    private static final int RESULT_OK = -1;
    private static final int RESULT_CANCELED = 0;
    private EditText mMessage, mNumber;
    private Button mShare, mMSG;
    private String WhatsAppMessage = null, WhatsNumber = null;
    private View mView = null;

    public WhatsAppFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_whatsapp, null);
        mView = view;
        mMessage = (EditText) view.findViewById(R.id.shareMessage);
        mNumber = (EditText) view.findViewById(R.id.shareNumber);
        mShare = (Button) view.findViewById(R.id.WhatsAppShare);
        mMSG = (Button) view.findViewById(R.id.WhatsAppMsg);

        mMSG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WhatsAppMessage = mMessage.getText().toString();
                WhatsNumber = mNumber.getText().toString();
                if (WhatsAppMessage.isEmpty() || WhatsNumber.isEmpty())
                    Toast.makeText(getActivity().getApplicationContext(), "Message or Number Cannot be empty!", Toast.LENGTH_SHORT).show();
                else
                    WhatsAppMsg(WhatsAppMessage, WhatsNumber);
            }
        });

        mShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WhatsAppMessage = mMessage.getText().toString();

                if (WhatsAppMessage.isEmpty())
                    Toast.makeText(getActivity().getApplicationContext(), "Message Cannot be empty!", Toast.LENGTH_SHORT).show();

                else {
                    postToWhatsApp(WhatsAppMessage);
                    mMessage.setText("");
                }
            }

        });


        return view;
    }

    //Send message to WhatsApp also
    public void postToWhatsApp(String status) {


        PackageManager pm = getActivity().getPackageManager();
        try {

            final Intent whatsAppIntent = new Intent(Intent.ACTION_SEND);
            whatsAppIntent.setType("text/plain");
            String text = status;

            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
            //Check if package exists or not. If not then code
            //in catch block will be called
            whatsAppIntent.setPackage("com.whatsapp");

            whatsAppIntent.putExtra(Intent.EXTRA_TEXT, text);
            //startActivity(Intent.createChooser(whatsAppIntent, "Share with"));
            startActivityForResult(whatsAppIntent, 5);

        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(getActivity().getApplicationContext(), "WhatsApp not Installed", Toast.LENGTH_SHORT)
                    .show();
        }


        getFragmentManager().popBackStack();
    }

    void WhatsAppMsg(String smsText, String smsNumber) {
        Uri uri = Uri.parse("smsto:" + smsNumber);
        Intent WhatsAppMsgIntent = new Intent(Intent.ACTION_SENDTO, uri);
        WhatsAppMsgIntent.putExtra(Intent.EXTRA_TEXT, smsText);
        WhatsAppMsgIntent.putExtra("sms_body", smsText);
        WhatsAppMsgIntent.setPackage("com.whatsapp");
        startActivity(WhatsAppMsgIntent);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 5 && resultCode == RESULT_OK) {
            Toast.makeText(getActivity().getApplicationContext(), "WhatsApp Message Posted Successfully", Toast.LENGTH_SHORT).show();
        } else if (requestCode == 5 && resultCode == RESULT_CANCELED)
            Toast.makeText(getActivity().getApplicationContext(), "You canceled posting to whatsapp!", Toast.LENGTH_SHORT).show();
    }
}
