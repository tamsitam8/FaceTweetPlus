package studio.epic.FaceTweetPlus;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;



public class LoginPage extends Activity implements View.OnClickListener {

    //Varriables for LoginPage.
    private SQLiteDatabase myDatabase;
    private SQLiteHelper mSqlLiteHelper;
    private TextView signUpLink;
    protected EditText textUser,textPass,currEmailTxt,oldPassTxt,newPassTxt;
    private static String currEmail = null,oldPass = null,newPass = null;
    final static String DATABASE_NAME = "MydataBase";
    public static String PREFS_NAME = "myPref";
    public static String PREF_USERNAME = "username";
    public static String PREF_PASSWORD = "password";
    public static String REMEMBER_CHECKED = "remember_checked";
    public static String REM_FUNC_CALLED = "remember_func_called";
    public CheckBox rememberMeCheckBox;
    private Button loginButton, forgotButton,changeButton;
    private static String remFuncIsCalled = "false";
    public static String isChecked = "false";
    public Dialog forgotPassDialog,changePassDialog;
    public EditText regEmailTxt;
    public static ImageView mProfile = null;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);

        myDatabase = openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);
        mSqlLiteHelper = new SQLiteHelper(this);

        //find UI elements.
        loginButton = (Button) findViewById(R.id.btn_login);
        forgotButton = (Button) findViewById(R.id.btn_forgot);
        changeButton = (Button) findViewById(R.id.chnagePass);
        textUser = (EditText) findViewById(R.id.input_Email);
        textPass = (EditText) findViewById(R.id.input_password);
        rememberMeCheckBox = (CheckBox) findViewById(R.id.rememberCheckBox);
        mProfile = (ImageView) findViewById(R.id.loginProfile);
        signUpLink = (TextView) findViewById(R.id.link_signup);




       //Making Buttons and Links clickable.
        signUpLink.setOnClickListener(this);
        loginButton.setOnClickListener(this);
        forgotButton.setOnClickListener(this);
        changeButton.setOnClickListener(this);

        //Bounce Animation to logo
        findViewById(R.id.loginProfile).clearAnimation();
        TranslateAnimation translation;
        translation = new TranslateAnimation(0f, 0f, getDisplayHeight(), 0f);
        translation.setStartOffset(500);
        translation.setDuration(2000);
        translation.setFillAfter(true);
        translation.setInterpolator(new BounceInterpolator());
        findViewById(R.id.loginProfile).startAnimation(translation);

        Log.d("zzz", "in onCreate Login onStartCalled = " + new HomeActivity().onStartCalled);
    }

    public void onStart() {
        super.onStart();

        //checking if user has signed out , dont display old profile picture then
        if (new HomeActivity().userSignedOut) {
            mProfile.setImageResource(R.drawable.login);
            return;
        }

        //read username and password from SharedPreferences
        remFuncIsCalled = getSharedPreferences(PREFS_NAME, MODE_PRIVATE)
                .getString(REM_FUNC_CALLED, "false");

        Log.d("aaa","in OnStart Login remFuncCalled = "+remFuncIsCalled);
        Log.d("aaa","in OnStart Login isChecked = "+isChecked);

        if (remFuncIsCalled.equals("true")) {
            isChecked = getSharedPreferences(PREFS_NAME, MODE_PRIVATE)
                    .getString(REMEMBER_CHECKED, "false");

            Log.d("aaa","in OnStart Login remFuncCalled true isChecked = "+isChecked);

            //Skips the LoginPage
            final String skipUser = getSharedPreferences(PREFS_NAME, MODE_PRIVATE)
                    .getString(PREF_USERNAME, "null");


            if (mSqlLiteHelper.authenticate(skipUser, mSqlLiteHelper.authRegEmail(skipUser))) {
                new GooglePlusFragment().isHLogOut = false;
                new GooglePlusFragment().gLoginResult = false;
                new HomeActivity().onStartCalled = 0;

                Log.d("TAG","in skipUser authenticate isHLogOut = "+new GooglePlusFragment().isHLogOut);
                Log.d("TAG","in skipUser authenticate gLoginResult = "+new GooglePlusFragment().gLoginResult);

                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                finish();
            } else
                Toast.makeText(LoginPage.this, "Authentication Problen", Toast.LENGTH_SHORT).show();


        }

            //for loading profile picture from storage
            //will do in future i guess :0
    }


     public void rememberMe(String user) {
        //save username and password in SharedPreferences
        getSharedPreferences(PREFS_NAME, MODE_PRIVATE)
                .edit()
                .putString(PREF_USERNAME, user)
                .putString(REMEMBER_CHECKED, "true")
                .putString(REM_FUNC_CALLED, "true")
                .commit();

        isChecked = getSharedPreferences(PREFS_NAME, MODE_PRIVATE)
                .getString(REMEMBER_CHECKED, "null");

    }


    //Method for Recients events of Buttons and Links.
    @Override
    public void onClick(View v) {

        int id = v.getId();

        if (id == R.id.link_signup)
            startActivity(new Intent(this, RegisterPage.class));

        if (id == R.id.btn_login) {

            if (!validate()) {
                onLoginFailed();
                return;
            }

            final ProgressDialog progressDialog = new ProgressDialog(LoginPage.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Authenticating...");
            progressDialog.show();


            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {

                            if (onLoginSuccess()) {
                                Toast.makeText(getApplicationContext(), "Welcome to FaceTweet!", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                                finish();
                            } else
                                onLoginFailed();

                            progressDialog.dismiss();
                        }
                    }, 3000);

        }


        if (id == R.id.btn_forgot)
            onForgot();

        if(id == R.id.chnagePass)
            onChangePassClicked();

        if (id == R.id.OkayButton) {
            final String regEmail = regEmailTxt.getText().toString();

            String authResult = null;
            authResult = mSqlLiteHelper.authRegEmail(regEmail);
            if (authResult == null) {
                Toast.makeText(LoginPage.this, "Sorry no registered user found by this email!", Toast.LENGTH_SHORT).show();
                forgotPassDialog.dismiss();
            } else {
                Toast.makeText(LoginPage.this, "Password = " + authResult, Toast.LENGTH_LONG).show();
                forgotPassDialog.dismiss();
            }
        }

        if (id == R.id.CancelButton) {
            forgotPassDialog.dismiss();
        }

        //for Changing password
        if(id == R.id.btn_changePass){
            currEmail = currEmailTxt.getText().toString();
            oldPass = oldPassTxt.getText().toString();
            newPass = newPassTxt.getText().toString();

            //User password is changed here after comparing the email and old password from Database.
            final SQLiteHelper.changePassEnum  changeEnum = mSqlLiteHelper.changeCurrentPass(currEmail, oldPass, newPass);
            if(changeEnum.equals(SQLiteHelper.changePassEnum.RESULT_SUCEESS))
            {
                Toast.makeText(this, "Password updated successfully", Toast.LENGTH_SHORT).show();
                changePassDialog.dismiss();
            }
            else if(changeEnum.equals(SQLiteHelper.changePassEnum.RESULT_NOT_INSERTED)){
                Toast.makeText(this, "Error in updating the current Password!", Toast.LENGTH_SHORT).show();
                changePassDialog.dismiss();
            }

            else if(changeEnum.equals(SQLiteHelper.changePassEnum.RESULT_AUTHFAIL)){
                Toast.makeText(this, "Username and Password doesn't match!", Toast.LENGTH_SHORT).show();
            }

            Log.d("qqq","in Login page changeEnum = "+changeEnum);
        }


    }

    public boolean onLoginSuccess() {

        new HomeActivity().userSignedOut = false;
        new GooglePlusFragment().isHLogOut = false;
        new GooglePlusFragment().gLoginResult = false;
        new HomeActivity().onStartCalled = 0;


        Log.d("TAG","in onLoginSuccess authenticate isHLogOut = "+new GooglePlusFragment().isHLogOut);
        Log.d("TAG","in onLoginSuccess authenticate gLoginResult = "+new GooglePlusFragment().gLoginResult);

        String Email = textUser.getText().toString();

        if (mSqlLiteHelper.authenticate(Email, Pass)) {

            if (rememberMeCheckBox.isChecked()) {
                rememberMe(Email); //save username in SharedPref.
            }

            else
                new HomeActivity().userSignedOut = true;

            return true;
        } else
            return false;

    }


    public boolean validate() {
        boolean valid = true;

        String email = textUser.getText().toString();
        String password = textPass.getText().toString();


        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            textUser.setError("enter a valid email address");
            valid = false;
        } else {
            textUser.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            textPass.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            textPass.setError(null);
        }

        return valid;

    }


    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed Error 0xCFFe", Toast.LENGTH_LONG).show();
        loginButton.setEnabled(true);
    }


    private int getDisplayHeight() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.widthPixels;
    }

    private void onForgot() {

        //Show the Alert Popup asking for Registered E-mail

        //Using Dialog Class
        forgotPassDialog = new Dialog(this);
        forgotPassDialog.setContentView(R.layout.forgort_layout);
        forgotPassDialog.setTitle("Forgot Pass?");
        regEmailTxt = (EditText) forgotPassDialog.findViewById(R.id.regEmail);
        Button okButton = (Button) forgotPassDialog.findViewById(R.id.OkayButton);
        Button cancelButton = (Button) forgotPassDialog.findViewById(R.id.CancelButton);

        okButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);

        forgotPassDialog.show();
    }


    //Method for changing password.
    private void onChangePassClicked() {

        //Show the Alert Popup asking for Registered E-mail and old password.

        //Using Dialog Class
        changePassDialog = new Dialog(this);
        changePassDialog.setContentView(R.layout.changepass_layout);
        changePassDialog.setTitle("Change Password?");

         currEmailTxt = (EditText) changePassDialog.findViewById(R.id.currentEmail);
         oldPassTxt = (EditText) changePassDialog.findViewById(R.id.oldPass);
         newPassTxt = (EditText) changePassDialog.findViewById(R.id.newPass);

        Button changePassBtn = (Button) changePassDialog.findViewById(R.id.btn_changePass);
        changePassBtn.setOnClickListener(this);
        changePassDialog.show();
    }

}


